package com.ion.spb.Models;

/**
 * JWTModel
 */
public class JWTModel {

    private String token;
    private User user;

    public JWTModel() {}

    public JWTModel(String token, User user) {
       this.token = token;
       this.user = user;
    }
    
    public User getUser() {
        return user;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }
}