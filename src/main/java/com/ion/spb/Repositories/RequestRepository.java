package com.ion.spb.Repositories;

import java.util.List;

import com.ion.spb.Models.Request;
import com.ion.spb.Models.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * RequestRepository
 */
@Repository
public interface RequestRepository extends JpaRepository<Request, Long> {
    List<Request> findByUser(User user);
    List<Request> findByStatus(Long status);
    List<Request> findByUserAndStatus(User user, Long status);
}