package com.ion.spb.Repositories;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import com.ion.spb.Models.Finalized;
import com.ion.spb.Models.Request;
import com.ion.spb.Models.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * UserRepository
 */
@Repository
public interface FinalizedRepository extends JpaRepository<Finalized, Long> {

    Optional<Finalized> findByid(int id);
    Optional<Finalized> findByRequest(Request request);
    List<Finalized> findByUserAndDate(User user, Date date);
}