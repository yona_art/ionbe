package com.ion.spb.Repositories;

import java.util.Optional;

import com.ion.spb.Models.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * UserRepository
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByfolio(String folio);
    
}