package com.ion.spb.Services;

import java.sql.SQLException;
import java.util.List;

import com.ion.spb.Models.Finalized;
import com.ion.spb.Models.Request;
import com.ion.spb.Repositories.RequestRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * RequestService
 */
@Service
public class RequestService {

    @Autowired
    private RequestRepository repository;

    @Autowired
    private FinalizedService finalizedService;

    @Autowired
    private UserService userService;

    public Request getOne(Long id) {
        return repository.getOne(id);
    }

    public List<Request> findAll() {
        return repository.findAll();
    }

    public List<Request> FindByStatus(Long status) {
        return repository.findByStatus(status);
    }

    public Request save(Request Request) {
        return repository.save(Request);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Finalized saveFinaled(Request Request, Long id) {
        Request newRequest = repository.save(Request);
        Finalized newFinalized = new Finalized("", userService.getOne(id), newRequest);
        return finalizedService.save(newFinalized);
    }

    public Request update(Request Request, Long id) {
        Request Requestedd = repository.getOne(id);
        Requestedd.setDescription(Request.getDescription());
        Requestedd.setLatitude(Request.getLatitude());
        Requestedd.setLongitude(Request.getLongitude());
        Requestedd.setUser(Request.getUser());
        Requestedd.setStatus(Request.getStatus());
        // Requestedd.setFinalized(Request.getFinalized());
        return repository.save(Requestedd);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Request updateByStatus(Long id, Long status) {
        Request requested = repository.getOne(id);
        requested.setStatus(status);
        return repository.save(requested);
    }

    public Boolean delete(Long id) {
        Request Request = repository.getOne(id);
        try {
            repository.delete(Request);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Transactional(isolation = Isolation.SERIALIZABLE, rollbackFor = { SQLException.class })
    public Request requests(Request Request) {
        return save(Request);
    }

}