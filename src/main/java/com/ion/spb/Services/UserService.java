package com.ion.spb.Services;

import java.util.List;

import com.ion.spb.Models.Request;
import com.ion.spb.Models.User;
import com.ion.spb.Repositories.RequestRepository;
import com.ion.spb.Repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * UserService
 */
@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private RequestRepository requestRepository;

    public User getOne(Long id) {
        return repository.getOne(id);
    }

    public List<User> findAll() {
        return repository.findAll();
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public List<Request> findRequestsbyUser(Long id, Long status) {
        User user = getOne(id);
        return requestRepository.findByUserAndStatus(user, status);
    }

    public User save(User user) {
        return repository.save(user);
    }

    public User update(User user, Long id) {
        User OldUser = repository.getOne(id);
        OldUser.setFolio(user.getFolio());
        OldUser.setName(user.getName());
        OldUser.setPassword(user.getPassword());
        OldUser.setPhone(user.getPhone());
        OldUser.setIsWorker(user.getIsWorker());
        // OldUser.setFinalized(user.getFinalized());
        return repository.save(OldUser);
    }

    public User findByFolio(String folio) throws NullPointerException {
        return repository.findByfolio(folio).get();
    }

    public Boolean delete(Long id) {
        User user = repository.getOne(id);
        try {
            repository.delete(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}