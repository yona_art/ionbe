package com.ion.spb.Services;

import java.util.List;

import com.ion.spb.Models.Finalized;
import com.ion.spb.Models.Request;
import com.ion.spb.Repositories.FinalizedRepository;
import com.ion.spb.Repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * FinalizedService
 */

@Service
public class FinalizedService {

    @Autowired
    private FinalizedRepository repository;

    @Autowired
    private UserRepository userRepository;

    public Finalized getOne(Long id) {
        return repository.getOne(id);
    }

    public List<Finalized> findAll() {
        return repository.findAll();
    }

    public Finalized save(Finalized finalized) {
        return repository.save(finalized);
    }

    public List<Finalized> findByUser(Long id) {
        return repository.findByUserAndDate(userRepository.getOne(id), null);
    }

    public Finalized findByRequest(Request request) {
        return repository.findByRequest(request).get();
    }

    public Boolean existsByRequest(Request Request) {
        return repository.findByRequest(Request).isPresent();
    }

    public Finalized update(Finalized finalized, Long id) {
        Finalized finalizededd = repository.getOne(id);
        finalizededd.setDescription(finalized.getDescription());
        finalizededd.setDate(finalized.getDate());
        finalizededd.setUser(finalized.getUser());
        finalized.setRequest(finalized.getRequest());
        return repository.save(finalizededd);
    }

    public Boolean delete(Long id) {
        Finalized finalized = repository.getOne(id);
        try {
            repository.delete(finalized);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}