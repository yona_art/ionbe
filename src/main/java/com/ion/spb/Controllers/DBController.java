package com.ion.spb.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * DBController
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("DB")
public class DBController {

    @Autowired
    private ResourceLoader resourceLoader;

    @GetMapping(value = "/SQL")
    public Resource  getMethodName() {
        return resourceLoader.getResource("classpath:static/LocalDB.sql");
    }

}