package com.ion.spb.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.List;

import com.ion.spb.Models.Finalized;
import com.ion.spb.Models.Request;
import com.ion.spb.Services.FinalizedService;
import com.ion.spb.Services.RequestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * RequestController
 */
@RestController
@RequestMapping(value = "Requests")
public class RequestController {

    @Autowired
    private RequestService service;

    @Autowired
    private FinalizedService finalizedService;

    @GetMapping(value = "/")
    public ResponseEntity<List<Request>> findAll() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Request> getById(@PathVariable Long id) {
        return new ResponseEntity<>(service.getOne(id), HttpStatus.OK);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Request> save(@RequestBody Request Request) {
        return new ResponseEntity<>(service.save(Request), HttpStatus.OK);
    }

    @PostMapping(value = "/{id}")
    public ResponseEntity<Finalized> saveFinalizedByRequest(@PathVariable Long id, @RequestBody Request Request) {
        return new ResponseEntity<>(service.saveFinaled(Request, id), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Request> update(@PathVariable Long id, @RequestBody Request Request) {
        return new ResponseEntity<>(service.update(Request, id), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}/status/{status}")
    public ResponseEntity<Request> getByStatusAndUserId(@PathVariable Long id, @PathVariable Long status) {
        if (status == 1L || status == 2L) {
            Finalized fn = finalizedService.findByRequest(service.getOne(id));
            fn.setDate(new Date(System.currentTimeMillis()));
            finalizedService.update(fn, fn.getId());
        }
        return new ResponseEntity<>(service.updateByStatus(id, status), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Boolean> detele(@PathVariable Long id) {
        return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
    }

}