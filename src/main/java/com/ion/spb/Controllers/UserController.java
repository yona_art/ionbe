package com.ion.spb.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.ion.spb.Models.Finalized;
import com.ion.spb.Models.Request;
import com.ion.spb.Models.User;
import com.ion.spb.Services.FinalizedService;
import com.ion.spb.Services.RequestService;
import com.ion.spb.Services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * UserController
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService service;

    @Autowired
    private RequestService requestService;

    @Autowired
    private FinalizedService finalizedService;

    @GetMapping(value = "/")
    public ResponseEntity<List<User>> findAll() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<User> getById(@PathVariable Long id) {
        return new ResponseEntity<>(service.getOne(id), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/Requests")
    public ResponseEntity<List<Finalized>> getbyUser(@PathVariable Long id,
            @RequestParam(name = "status") String status) {
        List<Request> items = requestService.FindByStatus(Long.parseLong(status));
        Finalized fin = new Finalized("", service.getOne(id), items.get(0));
        if (!finalizedService.existsByRequest(items.get(0))) {
            finalizedService.save(fin);
        }
        return new ResponseEntity<>(finalizedService.findByUser(id), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<User> update(@PathVariable Long id, @RequestBody User user) {
        return new ResponseEntity<>(service.update(user, id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Boolean> detele(@PathVariable Long id) {
        return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
    }

}