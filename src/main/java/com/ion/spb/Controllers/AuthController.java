package com.ion.spb.Controllers;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import com.ion.spb.Models.JWTModel;
import com.ion.spb.Models.User;
import com.ion.spb.Services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * AuthController
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("Auth")
public class AuthController {

	@Autowired
	private UserService service;

	@PostMapping(value = "/Register")
	public ResponseEntity<JWTModel> save(@RequestBody User user) {
		BCryptPasswordEncoder enconder = new BCryptPasswordEncoder();
		user.setPassword(enconder.encode(user.getPassword()).toString());
		User registered = service.save(user);
		return new ResponseEntity<>(new JWTModel(getJWTToken(registered.getPassword()), registered), HttpStatus.CREATED);
	}

	@PostMapping(value = "/Login")
	public ResponseEntity<JWTModel> login(@RequestBody User user) {
		PasswordEncoder enconder = new BCryptPasswordEncoder();
		User requestedUser = service.findByFolio(user.getFolio());
		if (enconder.matches(user.getPassword(), requestedUser.getPassword())) {
			return new ResponseEntity<JWTModel>(new JWTModel(getJWTToken(requestedUser.getPassword()), requestedUser),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
	}

	@PostMapping(value = "/")
	public ResponseEntity<Boolean> logout(HttpSession session){
		session.invalidate();
		return new ResponseEntity<>(true,HttpStatus.OK);
	}

	@GetMapping(value = "/")
	public ResponseEntity<Boolean> refresh(HttpSession session){
		return new ResponseEntity<>(true,HttpStatus.OK);
	}

	private String getJWTToken(String password) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
		String token = Jwts.builder()
		.setId("tokenID").setSubject(password)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();
		return "Bearer " + token;
	}

}