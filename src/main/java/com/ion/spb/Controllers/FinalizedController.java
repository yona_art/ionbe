package com.ion.spb.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.ion.spb.Models.Finalized;
import com.ion.spb.Services.FinalizedService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * UserController
 */

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("finalized")
public class FinalizedController {

    @Autowired
    private FinalizedService service;

    @GetMapping(value = "/")
    public ResponseEntity<List<Finalized>> findAll() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Finalized> getById(@PathVariable Long id) {
        return new ResponseEntity<>(service.getOne(id), HttpStatus.OK);
    }

    @PostMapping(value = "/")
    public ResponseEntity<Finalized> save(@RequestBody Finalized finalized) {
        return new ResponseEntity<>(service.save(finalized), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Finalized> update(@PathVariable Long id, @RequestBody Finalized finalized) {
        return new ResponseEntity<>(service.update(finalized, id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Boolean> detele(@PathVariable Long id) {
        return new ResponseEntity<>(service.delete(id), HttpStatus.OK);
    }

}